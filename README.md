# Dashboard React

Here We have a Dashbaord which contains a Navigation Bar and a set Of 4 Cards.

### Types of Cards
All the cards is placed in Dashboard.js which is located in client/src/views/Dashboard/Dashboard.js
Pie Chart Components are located in client/src/views/Pie1, Pie2, Pie3 
  - Reviews (Line no. 162 to 208)
  - AV. Rating (Line no. 211 to 233)
  - Analysis (Line no. 236 to 261)
  - Stock Sold (Line no. 266 to 275)

#### Reviews

  Total number of reviews are shown with the contact icons placed and also with the information of no. of positive and negative reviews.
  
#### AV. Rating  

 Card showing the AV. Rating and also the total customer rating monthly in Bar Graph which is placed.
 
#### Analysis

In this card we have placed 3 different Pie Chart Components namely - Positive, Neutral, Negative Reviews.

#### Stock Sold

In this card we have placed a line graph showing the % stock sold monthwise.

#### Navigation Bar

Consisting of several other contents.

### Work Distribution

Front End Part is done by Ashish and Atul
- 2 Cards Analysis and Stock Sold graph components are made by Ashish
- 2 Cards Reviews and AV. Rating is made by Atul
- SideBar and Styling is made by both 

Back End Part is done by Nishant
- Total No. of reviews and AV. Rating consist of the data coming from the backend
- LineChart Component consists of a graph which is being made according to the data coming from the backend
When Form is submitted the data is shown accordingly on the Dashboard

##### Thanks For Reviewing