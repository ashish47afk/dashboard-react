//Importing icons from Material UI
import Dashboard from "@material-ui/icons/Dashboard";
import WidgetsIcon from '@material-ui/icons/Widgets';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import PersonIcon from '@material-ui/icons/Person';
import TimelineOutlinedIcon from '@material-ui/icons/TimelineOutlined';
import StorageIcon from '@material-ui/icons/Storage';

//importing Dashboard and Form Component
import DashboardPage from "views/Dashboard/Dashboard.js";
import Form from "views/Form/Form.js";
const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/widget",
    name: "Widget",
    icon: WidgetsIcon,
    component: Form,
    layout: "/admin"
  },
  {
    path: "/reviews",
    name: "Reviews",
    icon: StarBorderOutlinedIcon,
    component: Form,
    layout: "/admin"
  },
  {
    path: "/customer",
    name: "Customer",
    icon: PersonIcon,
    component: Form,
    layout: "/admin"
  },
  {
    path: "/analysis",
    name: "Online Analysis",
    icon: TimelineOutlinedIcon,
    component: Form,
    layout: "/admin"
  },
  {
    path: "/form",
    name: "Form",
    icon: StorageIcon,
    component: Form,
    layout: "/admin"
  }
];

export default dashboardRoutes;
